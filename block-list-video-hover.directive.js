(function () {
    'use strict';

    function blockListVideoHover (PATH_CONFIG) {
        return {
            restrict: 'E',
            replace: false,

            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

            },

            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-list-video-hover/block-list-video-hover.html'

        }
    }

    blockListVideoHover.$inject = ['PATH_CONFIG'];

    angular
        .module('bravoureAngularApp')
        .directive('blockDoubleVideo', blockListVideoHover);

})();
