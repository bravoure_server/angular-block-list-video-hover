# Bravoure - Block List Video Hover

## Directive: Code to be added:

        <block-list-video-hover></block-list-video-hover>

## Use


    $scope.kind = 'video-hover';
    // Loads the slider item.



### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-block-list-video-hover": "1.0.1"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
